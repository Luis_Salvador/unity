﻿using UnityEngine;
using shop;

public class NewBehaviourScript : MonoBehaviour
{
    static string next;

    static Console[] consoles;
    static Game[] games;

    static float total;

    // Start is called before the first frame update
    void Start()
    {
        consoles = new Console[3];
        consoles[0] = new Playstation("Playstation 4", Playstation.Type.Playstation_4, 1500);
        consoles[1] = new Nintendo("Switch mni", Nintendo.Type.Switch, 1200);
        consoles[2] = new Xbox("Xbox 360 Halo edition", Xbox.Type.Xbox_360, 800);

        games = new Game[2];
        games[0] = new Digital("Fallout 4", Digital.Platform.Steam, 50);
        games[1] = new Physical("God of War", 60);
       
        Loop();
    }

    static void Loop()
    {
        Debug.Log("Consoles");
        Debug.Log("========");
        for (int i = 0; i < consoles.Length; i++)
            Debug.Log("\t" + (1 + i) + " - " + consoles[i].name + " S/." + consoles[i].price);

        Debug.Log("Games");
        Debug.Log("=====");
        for (int i = 0; i < games.Length; i++)
            Debug.Log("\t" + (4 + i) + " - " + games[i].name + " S/." + games[i].price);

        string str = "4";
        int option = int.Parse(str);
        if (option < 4)
            total += consoles[option - 1].price;
        else if (option < 6)
            total += games[option - 4].price;

        Debug.Log("Total: " + total);
    }
}
