﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shop
{
    class Nintendo : Console
    {
        public enum Type
        {
            Nintendo_3DS,
            Switch
        }

        public Nintendo(string name, Type type, float price) : base(name, price)
        {
        }

        public void InsertCartrige() { }
    }
}
