﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shop
{
    class Playstation : Console
    {
        public enum Type
        {
            Playstation_3,
            Playstation_4,
            Playstation_4_pro
        }

        public Type type;

        public Playstation(string name, Type type, float price) : base(name, price)
        {
            this.type = type;
        }

        public void ConnectPsMove() { }
    }
}
