﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shop
{
    class Digital: Game
    {
        public enum Platform
        {
            Playstation_Store,
            EShop,
            Steam
        }

        public Platform platform;

        public Digital (string name, Platform platform, float price) : base(name, price)
        {
            this.platform = platform;
        }
    }
}
