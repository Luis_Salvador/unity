﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shop
{
    class Xbox : Console
    {
        public enum Type
        {
            Xbox_360,
            Xbox_One,
        }

        public Xbox(string name, Type tyoe, float price) : base(name, price)
        {
        }

        public void ConnectKinect() { }
    }
}
