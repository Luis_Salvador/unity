﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shop;

namespace shop
{
    class Program
    {
        static string next;

        static shop.Console[] consoles;
        static Game[] games;

        static float total;

        static void Main(string[] args)
        {
            bool loop = true;

            consoles = new shop.Console[3];
            consoles[0] = new Playstation("Playstation 4", Playstation.Type.Playstation_4, 1500);
            consoles[1] = new Nintendo("Switch mni", Nintendo.Type.Switch, 1200);
            consoles[2] = new Xbox("Xbox 360 Halo edition", Xbox.Type.Xbox_360, 800);

            games = new Game[2];
            games[0] = new Digital("Fallout 4", Digital.Platform.Steam, 50);
            games[1] = new Physical("God of War", 60);

            while (loop)
            {
                System.Console.Clear();

                Loop();

                System.Console.WriteLine("Continue?? (Y/N)");
                next = System.Console.ReadLine();
                loop = (next.ToUpper() == "Y");
            }
        }

        static void Loop()
        {
            System.Console.WriteLine("Consoles");
            System.Console.WriteLine("========");
            for (int i = 0; i < consoles.Length; i++)
                System.Console.WriteLine("\t" + (1 + i) + " - " + consoles[i].name + " S/." + consoles[i].price);

            System.Console.WriteLine("Games");
            System.Console.WriteLine("=====");
            for (int i = 0; i < games.Length; i++)
                System.Console.WriteLine("\t" + (4 + i) + " - " + games[i].name + " S/." + games[i].price);

            string str = System.Console.ReadLine();
            int option = Int32.Parse(str);
            if (option < 4)
                total += consoles[option - 1].price;
            else if (option < 6)
                total += games[option - 4].price;
            else
                System.Console.WriteLine("Total: " + total);
        }
    }
}
